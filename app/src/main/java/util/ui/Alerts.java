package util.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.juan.darkskytest.R;


/**
 * Created by juan on 31/08/16.
 */
public class Alerts {

    private Context context;

    public Alerts(Context context) {
        this.context = context;
    }

    public void toast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public Dialog confirmAlert(String title, String message, View.OnClickListener accept, View.OnClickListener cancel, boolean cancelable) {
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.confirm_alert);
        dialog.setTitle(title);
        final TextView text = (TextView) dialog.findViewById(R.id.text_alert);
        text.setText(message);
        dialog.setCancelable(cancelable);
        dialog.show();

        Button acceptButton = (Button) dialog.findViewById(R.id.accept);
        acceptButton.setOnClickListener(accept);

        Button close = (Button) dialog.findViewById(R.id.cancel_alert);
        close.setOnClickListener(cancel);
        return dialog;
    }

    public Dialog alert(String title, String message, View.OnClickListener accept, boolean cancelable) {
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.alert);
        dialog.setTitle(title);
        final TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(message);
        dialog.setCancelable(cancelable);
        dialog.show();
        Button acceptButton = (Button) dialog.findViewById(R.id.accept);
        acceptButton.setOnClickListener(accept);
        return dialog;
    }

    public Dialog alertWithoutListener(String title, String message, boolean cancelable) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.alert);
        dialog.setTitle(title);
        final TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(message);
        dialog.setCancelable(cancelable);
        dialog.show();
        Button acceptButton = (Button) dialog.findViewById(R.id.accept);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }


    public ProgressDialog initProgressDialog(String title, String message, ProgressDialog pd) {
        pd = ProgressDialog.show(context, title, message, true, false);
        return pd;
    }

    public void closeProgressDialog(ProgressDialog pd) {
        try {
            pd.dismiss();
        } catch (Exception e) {
        }
    }
}
