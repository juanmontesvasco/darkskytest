package util.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by juan on 13/07/17.
 */

public class Date {

    public static String getDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy");
        String date = df.format(c.getTime());
        return date;
    }


}
