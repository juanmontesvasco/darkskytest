package model;

import com.juan.darkskytest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import config.Parameters;
import util.date.Date;

/**
 * Created by juan on 13/07/17.
 */

public class Result {
    private String timezone;
    private String date;
    private String summary;
    private String windSpeed;
    private String angle;
    private String humidity;
    private int background;
    private Weather current;
    private Weather monday;
    private Weather tuesday;
    private Weather Wednesday;
    private Weather thursday;
    private Weather friday;

    public Result() {
    }


    public Result(String timezone, String date, String summary, String windSpeed, String angle, String humidity, Weather current, Weather monday, Weather tuesday, Weather wednesday, Weather thursday, Weather friday) {
        this.timezone = timezone;
        this.date = date;
        this.summary = summary;
        this.windSpeed = windSpeed;
        this.angle = angle;
        this.humidity = humidity;
        this.current = current;
        this.monday = monday;
        this.tuesday = tuesday;
        this.Wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getWindSpeed() {
        return windSpeed + " MPH";
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getAngle() {
        if (Double.valueOf(angle) <= 180) {
            return Parameters.north;
        } else {
            return Parameters.south;
        }
    }

    public void setAngle(String angle) {
        this.angle = angle;
    }

    public String getHumidity() {
        return humidity + "%";
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public Weather getCurrent() {
        return current;
    }

    public void setCurrent(Weather current) {
        this.current = current;
    }

    public Weather getMonday() {
        return monday;
    }

    public void setMonday(Weather monday) {
        this.monday = monday;
    }

    public Weather getTuesday() {
        return tuesday;
    }

    public void setTuesday(Weather tuesday) {
        this.tuesday = tuesday;
    }

    public Weather getWednesday() {
        return Wednesday;
    }

    public void setWednesday(Weather wednesday) {
        Wednesday = wednesday;
    }

    public Weather getThursday() {
        return thursday;
    }

    public void setThursday(Weather thursday) {
        this.thursday = thursday;
    }

    public Weather getFriday() {
        return friday;
    }

    public void setFriday(Weather friday) {
        this.friday = friday;
    }


    public void parseJson(JSONObject json) throws JSONException {
        JSONObject currently = json.getJSONObject("currently");
        JSONObject daily = json.getJSONObject("daily");
        JSONArray data = daily.getJSONArray("data");
        List<Weather> days = new ArrayList<Weather>();
        for (int i = 0; i < data.length(); i++) {
            days.add(new Weather(data.getJSONObject(i).getString("temperatureMax"),
                    data.getJSONObject(i).getString("icon")));
        }
        Weather current = new Weather(currently.getString("temperature"),
                currently.getString("icon"));
        String timezone = json.getString("timezone");
        String summary = currently.getString("summary");
        String windSpeed = currently.getString("windSpeed");
        String humidity = currently.getString("humidity");
        String angle = currently.getString("windBearing");
        String date = Date.getDate();
        this.setTimezone(timezone);
        this.setDate(date);
        this.setSummary(summary);
        this.setWindSpeed(windSpeed);
        this.setAngle(angle);
        this.setHumidity(humidity);
        this.setCurrent(current);
        this.setMonday(days.get(0));
        this.setTuesday(days.get(1));
        this.setWednesday(days.get(2));
        this.setThursday(days.get(3));
        this.setFriday(days.get(4));
    }
}
