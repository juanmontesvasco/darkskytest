package model;

import config.Parameters;

import com.juan.darkskytest.R;

/**
 * Created by juan on 13/07/17.
 */

public class Weather {
    private String temperature;
    private String icon;
    private int drawableIcon;
    private int backDrawableIcon;


    public Weather(String temperature, String icon) {
        this.setTemperature(temperature);
        this.setIcon(icon);
        this.setIconId(icon);
    }

    public String getTemperature() {
        return temperature + "°";
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }


    public int getDrawableIcon() {
        return drawableIcon;
    }

    public void setDrawableIcon(int drawableIcon) {
        this.drawableIcon = drawableIcon;
    }


    public int getBackDrawableIcon() {
        return backDrawableIcon;
    }

    public void setBackDrawableIcon(int backDrawableIcon) {
        this.backDrawableIcon = backDrawableIcon;
    }

    public void setIconId(String icon) {
        switch (icon) {
            case Parameters.clearDay:
                this.setDrawableIcon(R.drawable.sun);
                this.setBackDrawableIcon(R.drawable.sun_background);
                break;
            case Parameters.clearNight:
                this.setDrawableIcon(R.drawable.clear_night);
                this.setBackDrawableIcon(R.drawable.clear_night_background);
                break;
            case Parameters.rain:
                this.setDrawableIcon(R.drawable.rain);
                this.setBackDrawableIcon(R.drawable.rain_background);
                break;
            case Parameters.snow:
                this.setDrawableIcon(R.drawable.snow);
                this.setBackDrawableIcon(R.drawable.snow_background);
                break;
            case Parameters.sleet:
                this.setDrawableIcon(R.drawable.sleet);
                this.setBackDrawableIcon(R.drawable.sleet_background);
                break;
            case Parameters.wind:
                this.setDrawableIcon(R.drawable.wind);
                this.setBackDrawableIcon(R.drawable.win_background);
                break;
            case Parameters.fog:
                this.setDrawableIcon(R.drawable.fog);
                this.setBackDrawableIcon(R.drawable.fog_background);
                break;
            case Parameters.cloudy:
                this.setDrawableIcon(R.drawable.cloudy);
                this.setBackDrawableIcon(R.drawable.cloudy_background);
                break;
            case Parameters.partlyCloudyDay:
                this.setDrawableIcon(R.drawable.partly_cloudy_day);
                this.setBackDrawableIcon(R.drawable.cloudy_day_background);
                break;
            case Parameters.partlyCloudyNight:
                this.setDrawableIcon(R.drawable.partly_cloudy_night);
                this.setBackDrawableIcon(R.drawable.cloudy_night_background);
                break;
            case Parameters.hail:
                this.setDrawableIcon(R.drawable.hail);
                this.setBackDrawableIcon(R.drawable.hail_backgound);
                break;
            case Parameters.thunderstorm:
                this.setDrawableIcon(R.drawable.thunderstorm);
                this.setBackDrawableIcon(R.drawable.thunder_background);
                break;
            case Parameters.tornado:
                this.setDrawableIcon(R.drawable.tornado);
                this.setBackDrawableIcon(R.drawable.tornado_background);
                break;
            default:
                this.setDrawableIcon(R.drawable.sun);
                this.setBackDrawableIcon(R.drawable.sun_background);
                break;
        }
    }

}
