package config;

/**
 * Created by juan on 13/07/17.
 */

public class Parameters {
    private static final String SECRET_KEY = "f3cc6519227794fbd60f47c2e38b1e53";
    public static final String SERVER = "https://api.darksky.net/forecast/" + SECRET_KEY;

    public static final String clearDay = "clear-day";
    public static final String clearNight = "clear-night";
    public static final String rain = "rain";
    public static final String snow = "snow";
    public static final String sleet = "sleet";
    public static final String wind = "wind";
    public static final String fog = "fog";
    public static final String cloudy = "cloudy";
    public static final String partlyCloudyDay = "partly-cloudy-day";
    public static final String partlyCloudyNight = "partly-cloudy-night";
    public static final String hail = "hail";
    public static final String thunderstorm = "thunderstorm";
    public static final String tornado = "tornado";
    public static final String south = "SOUTH";
    public static final String north = "NORTH";
}
