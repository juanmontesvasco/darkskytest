package com.juan.darkskytest;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import config.Parameters;
import model.Result;
import util.network.Requests;
import util.storage.DeviceStorage;
import util.ui.Alerts;
import util.validate.Validate;

public class Main extends AppCompatActivity {

    private static final String[] PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private Context context;
    private Alerts alerts;
    private static final int REQUEST = 2301;
    private LocationListener locListener;
    private ProgressDialog pd;
    private LocationManager locManager;
    private DeviceStorage storage;
    private boolean located = false, lastKnown = false;
    private Requests requests;
    @BindView(R.id.current_temperature)
    TextView currentTemperature;
    @BindView(R.id.summary)
    TextView summary;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.wind)
    TextView wind;
    @BindView(R.id.compass)
    TextView compass;
    @BindView(R.id.humidity)
    TextView humidity;
    @BindView(R.id.mon)
    TextView mon;
    @BindView(R.id.tue)
    TextView tue;
    @BindView(R.id.wed)
    TextView wed;
    @BindView(R.id.thu)
    TextView thu;
    @BindView(R.id.fri)
    TextView fri;
    @BindView(R.id.mon_icon)
    ImageView monIcon;
    @BindView(R.id.tue_icon)
    ImageView tueIcon;
    @BindView(R.id.wed_icon)
    ImageView wedIcon;
    @BindView(R.id.thu_icon)
    ImageView thuIcon;
    @BindView(R.id.fri_icon)
    ImageView friIcon;
    @BindView(R.id.current_icon)
    ImageView currentIcon;
    @BindView(R.id.background_image)
    ImageView backgroundImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initObjects();
        verifyPermission();
    }


    private void initObjects() {
        context = this;
        alerts = new Alerts(context);
        storage = new DeviceStorage(context);
        requests = new Requests(context);
        if (!Validate.verifyGPS(context)) {
            alerts.alertWithoutListener(getResources().getString(R.string.error),
                    getResources().getString(R.string.no_gps),
                    true);
        }
    }

    private void initLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        Location loc = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (loc != null) {
            lastKnown = true;
            showLocation(loc);
            getDataFromDark();
        }
        locListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                locManager.removeUpdates(locListener);
                showLocation(location);
                if (!lastKnown) {
                    lastKnown = true;
                    getDataFromDark();
                }
            }

            public void onProviderDisabled(String provider) {
            }

            public void onProviderEnabled(String provider) {
                if (!located) {
                    initLocation();
                }
            }

            public void onStatusChanged(String provider, int status,
                                        Bundle extras) {
            }
        };
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                locListener);
    }


    private void showLocation(Location loc) {
        storage.save("lat", loc.getLatitude());
        storage.save("lng", loc.getLongitude());
        located = true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void verifyPermission() {
        if (!canAccessLocation() || !canAccessWrite()) {
            requestPermissions(PERMS, REQUEST);
        } else {
            initLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST:
                if (canAccessWrite() && canAccessLocation()) {
                    initLocation();
                } else {
                    alerts.alertWithoutListener(getResources().getString(R.string.error),
                            getResources().getString(R.string.enable_permissions),
                            false);
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessWrite() {
        return (hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission(String perm) {
        try {
            return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, perm));
        } catch (Exception e) {
            return true;
        }
    }

    private void getDataFromDark() {
        if (Validate.hasInternet(context)) {
            if (located) {
                initPd();
                Response.Listener<String> success = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dismissPd();
                        Result result = new Result();
                        try {
                            result.parseJson(new JSONObject(response));
                            showInfo(result);
                        } catch (JSONException e) {
                            alerts.alertWithoutListener(getResources().getString(R.string.error),
                                    getResources().getString(R.string.error_message), true);
                        }
                    }
                };

                Response.ErrorListener error = new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissPd();
                        alerts.alertWithoutListener(getResources().getString(R.string.error),
                                getResources().getString(R.string.error_message), true);
                    }
                };
                requests.get(Parameters.SERVER + "/" + storage.get("lat") + "," + storage.get("lng"),
                        requests.getDefaultHeaders(),
                        success,
                        error
                );
            } else {
                if (!Validate.verifyGPS(context)) {
                    alerts.alertWithoutListener(getResources().getString(R.string.error),
                            getResources().getString(R.string.no_gps),
                            true);
                }
            }
        } else {
            alerts.toast(getResources().getString(R.string.no_internet));
        }
    }

    private void initPd() {
        pd = ProgressDialog.show(context, getResources().getString(R.string.one_second),
                getResources().getString(R.string.make_request), true, false);
    }

    private void dismissPd() {
        try {
            pd.dismiss();
        } catch (Exception e) {
        }
    }

    private void showInfo(Result result) {
        alerts.toast(getResources().getString(R.string.updated));
        mon.setText(result.getMonday().getTemperature());
        monIcon.setImageDrawable(ContextCompat.getDrawable(context, result.getMonday().getDrawableIcon()));

        tue.setText(result.getTuesday().getTemperature());
        tueIcon.setImageDrawable(ContextCompat.getDrawable(context, result.getTuesday().getDrawableIcon()));


        wed.setText(result.getWednesday().getTemperature());
        wedIcon.setImageDrawable(ContextCompat.getDrawable(context, result.getWednesday().getDrawableIcon()));


        thu.setText(result.getThursday().getTemperature());
        thuIcon.setImageDrawable(ContextCompat.getDrawable(context, result.getThursday().getDrawableIcon()));


        fri.setText(result.getFriday().getTemperature());
        friIcon.setImageDrawable(ContextCompat.getDrawable(context, result.getFriday().getDrawableIcon()));

        summary.setText(result.getSummary());
        currentTemperature.setText(result.getCurrent().getTemperature());
        wind.setText(result.getWindSpeed());
        humidity.setText(result.getHumidity());
        date.setText(result.getDate());
        compass.setText(result.getAngle());
        currentIcon.setImageDrawable(ContextCompat.getDrawable(context, result.getCurrent().getDrawableIcon()));
        backgroundImage.setImageDrawable(ContextCompat.getDrawable(context, result.getCurrent().getBackDrawableIcon()));
        this.getSupportActionBar().setTitle(result.getTimezone());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.update) {
            getDataFromDark();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
